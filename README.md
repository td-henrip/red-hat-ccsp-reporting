# Red Hat CCSP reporting

This project is just Proof of Concept how to generate Red Hat CCSP monthly reports from Satellite server. 
This Includes simple Bash script and documentation to create proper Host Collections into Satellite server.

# Short guide

You need to have a Red Hat Satellite server installed and configured. It does not matter if you have already registered systems on it.
You also need to know Red Hat CCSP SKU's you are going to report (or have previously reported).


1. Create host-collections, name them like "SKU: RH12345" and have all your SKU's on separate host-collections you are reporting.
2. Attach these host-collections to systems which are consuming such subscriptions.
3. Create host-groups for each customer, name them like "Customer: Hosted Customer Alpha". If 
4. Have each system attached  into one of these host-groups.
5. On your Satellite CLI (over ssh?), create .ccsp -subdirectory and place template files there.
6. Place script into you $PATH and edit it to have your company information (company name, address etc)
7. Have your Satellite account ready, it is needed with script

When systems are attached and files in place, you can run script on command line. It will output into stdout your report so it can be redirected to email or into web folder.


`$ ccsp-report.sh | tee /var/www/html/202107-report.csv`
